#include <Keyboard.h>
#include <SoftwareSerial.h>

// Bluetooth pins
int g_rx_pin = 10;
int g_tx_pin = 11;

// Relay pins
int g_first_relay_pin = 5;
int g_second_relay_pin = 6;

// LED pins

int g_green_led_pin = 2;
int g_red_led_pin = 3;

// Keys for enabling and disabling power socket (receive them via bluetooth)
const char* g_enable_key = "BTotj3LDmscaTK-1\r\n";
const char* g_disable_key = "BTotj3LDmscaTK-0\r\n";
int g_key_length = 18;

SoftwareSerial BTSerial(g_rx_pin, g_tx_pin);

void enable_red_led()
{
  digitalWrite(g_green_led_pin, LOW);
  digitalWrite(g_red_led_pin, HIGH);
}

void enable_green_led()
{
  digitalWrite(g_red_led_pin, LOW);
  digitalWrite(g_green_led_pin, HIGH);
}

void setup(void) 
{
  pinMode(g_green_led_pin, OUTPUT);
  pinMode(g_red_led_pin, OUTPUT);
  enable_red_led();
  
  pinMode(g_first_relay_pin, OUTPUT);
  pinMode(g_second_relay_pin, OUTPUT);
  digitalWrite(g_first_relay_pin, HIGH); // Turn off the first relay channel
  digitalWrite(g_second_relay_pin, HIGH); // Turn off the second relay channel
  
  pinMode(g_rx_pin, INPUT);
  pinMode(g_tx_pin, OUTPUT);
  BTSerial.begin(38400);
}

void loop(void)
{
  int i = 0;
  char recv_buff[19] = {0};
  
  bool can_read_sth = BTSerial.available();
  if (can_read_sth)
  {
    do
    {
      if (i > g_key_length)
      {
        break;
      }
      
      recv_buff[i++] = BTSerial.read();
      //This delay helps us to read everything, cause processor is much faster then the baud rate
      delay(10);
    } while (BTSerial.available() > 0);

    if (strcmp(recv_buff, g_enable_key) == 0)
    {
      enable_green_led();
	  
	  // Enable relay pins. LOW, cause they are inverted
      digitalWrite(g_first_relay_pin, LOW);
      digitalWrite(g_second_relay_pin, LOW);
	  
	  // Log in to the Windows
      Keyboard.press(0xB1); // Pressing ESC
      delay(500);
      Keyboard.print("111111"); // Your password
      Keyboard.press(0xB0); // Pressing enter
      delay(50);
      Keyboard.releaseAll();
      delay(100);
      Keyboard.end();
      delay(1000);
    }
    else if (strcmp(recv_buff, g_disable_key) == 0)
    {
      enable_red_led();
      digitalWrite(g_first_relay_pin, HIGH);
      digitalWrite(g_second_relay_pin, HIGH);
      
	  // Log off from the Windows
      Keyboard.begin();
      Keyboard.press(KEY_LEFT_GUI);
      Keyboard.print("l");
      delay(100);
      Keyboard.releaseAll();
      Keyboard.end();
      delay(1000);
    }
  }
}
