#include <SoftwareSerial.h>
#include <SPI.h>
#include <MFRC522.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

// Bluetooth pins
short g_rx_pin = 8;
short g_tx_pin = 9;

/** RFID pins
* Pinout for Arduino UNO:
* RST - Any digital PIN
* NSS (SDA) - Any digital PIN
* IRQ (if exists) - None
* SCK - ICSP-3
* MISO - ICSP-1
* MOSI - ICSP-4
**/
short g_rst_pin = 5;
short g_nss_pin = 4;

short g_last_active_switch = 0;

short g_audio_pin = 6;
short g_switch_left_pin = 2;
short g_switch_right_pin = 3;

bool g_last_sent_state = true;

// Keys for enabling and disabling power socket (send them via bluetooth)
const char* g_enable_key = "BTotj3LDmscaTK-1";
const char* g_disable_key = "BTotj3LDmscaTK-0";

// 128-byte secret key, that allows you to enable power socket (must be written on g_rfid card)
// We do not write nul-terminators, so we can use 16 bytes for secret key piece
const short g_key_total_blocks = 8;
const short g_key_block_size = 16;
byte g_secret_key[g_key_total_blocks][g_key_block_size + 1] = {
  "RSvcu5%BlSdYbDOb",
  "1sa12FVuNhAVFVpV",
  "5c$v7G^wR*yo-hsr",
  "wd3EubJDtR#AKUL9",
  "5INmQ&fSsDAyDqw5",
  "QNIc2WdUg+t7KCW4",
  "HRi@0ATLFte1O=LE",
  "WSp:pFlxlPQ7ds9U"
};
byte g_key_block_numbers[g_key_total_blocks] = {4, 5, 6, 8 ,9, 10, 12, 13};
byte g_key_trailer_block_numbers[g_key_total_blocks] = {7, 7, 7, 11, 11, 11, 15, 15};

SoftwareSerial g_bt_serial(g_rx_pin, g_tx_pin);
MFRC522 g_rfid(g_nss_pin, g_rst_pin);
MFRC522::MIFARE_Key g_rfid_key;
LiquidCrystal_I2C g_lcd(0x27,16,2);

signed short rfid_authenticate_key(const short key_type, const short trailer_block)
{
  MFRC522::StatusCode status_code = (MFRC522::StatusCode)g_rfid.PCD_Authenticate(key_type, trailer_block, &g_rfid_key, &(g_rfid.uid));
  return status_code == MFRC522::STATUS_OK ? 0 : -1;
}

signed short verify_secret_key_block(const byte *block_data, unsigned int block_size, unsigned short block_num)
{
  if (block_num >= g_key_total_blocks || block_size < g_key_block_size)
  {
    return -1;
  }

  for (int i = 0; i < g_key_block_size; i++)
  {
    if (g_secret_key[block_num][i] != block_data[i])
    {
      return -1;
    }
  }
}

signed short rfid_read_secret_key()
{
  for (int i= 0; i < g_key_total_blocks; i++)
  {
    if (rfid_authenticate_key(MFRC522::PICC_CMD_MF_AUTH_KEY_A, g_key_trailer_block_numbers[i]) < 0)
    {
      return -1;
    }

    byte data_size = g_key_block_size + 2;
    byte data[data_size];    
    MFRC522::StatusCode status_code = (MFRC522::StatusCode)g_rfid.MIFARE_Read(g_key_block_numbers[i], data, &data_size);
    if (status_code != MFRC522::STATUS_OK)
    {
      return -1;
    }

    if (verify_secret_key_block(data, data_size, i) < 0)
    {
      return -1;
    }
  }
  
  return 0;
}

signed short rfid_write_secret_key(bool reset)
{
  byte reset_data[g_key_block_size + 1] = "0000000000000000";
  for (int i = 0; i < g_key_total_blocks; i++)
  {
    if (rfid_authenticate_key(MFRC522::PICC_CMD_MF_AUTH_KEY_B, g_key_trailer_block_numbers[i]) < 0)
    {
      return -1;
    }

    MFRC522::StatusCode status_code = (MFRC522::StatusCode)g_rfid.MIFARE_Write(g_key_block_numbers[i], reset ? (byte *)reset_data : g_secret_key[i], g_key_block_size); 
    if (status_code != MFRC522::STATUS_OK)
    {
      return -1;
    }
  }

  return 0;
}

signed short rfid_wait_card()
{
  // Look for new cards
  if ( ! g_rfid.PICC_IsNewCardPresent())
    return -1;  
  // Verify if the NUID has been read
  if ( ! g_rfid.PICC_ReadCardSerial())
    return -1;
  MFRC522::PICC_Type picc_type = g_rfid.PICC_GetType(g_rfid.uid.sak);
  // Check for compatibility
  if (picc_type != MFRC522::PICC_TYPE_MIFARE_MINI
        &&  picc_type != MFRC522::PICC_TYPE_MIFARE_1K
        &&  picc_type != MFRC522::PICC_TYPE_MIFARE_4K)
  {
    return -1;
  }

  return 0;
}

void lcd_display_two_lines(const char* first_line, const char* second_line)
{
  g_lcd.clear();
  g_lcd.setCursor(0, 0);
  g_lcd.print(first_line);
  g_lcd.setCursor(0, 1);
  g_lcd.print(second_line);
}

int get_active_switch_side()
{
  int left_side_enabled = digitalRead(g_switch_left_pin);
  return left_side_enabled == HIGH ? 2 : 3;
}

void play_sound(bool success)
{
  if (success)
  {
    tone(g_audio_pin, 1000, 300);
    delay(400);
    tone(g_audio_pin, 1150, 300);
    delay(350);
    tone(g_audio_pin, 1150, 300);
    delay(350);
  }
  else
  {
    tone(g_audio_pin, 500, 300);
    delay(500);
    tone(g_audio_pin, 500, 300);
    delay(500);
  }
}

void setup(void) 
{
  SPI.begin(); // Init SPI bus
  g_rfid.PCD_Init(); // Init MFRC522

  g_lcd.init();                     
  g_lcd.backlight();

  pinMode(g_switch_left_pin, INPUT);
  pinMode(g_switch_right_pin, INPUT);
  pinMode(g_switch_left_pin, INPUT_PULLUP);
  pinMode(g_switch_right_pin, INPUT_PULLUP);
  
  pinMode(g_rx_pin, INPUT);
  pinMode(g_tx_pin, OUTPUT);
  g_bt_serial.begin(38400);

  pinMode(g_audio_pin, OUTPUT);

  // Prepare the RFID key (using both as key A and as key B)
  // using FFFFFFFFFFFFh which is the default from the manufacturer
  for (byte i = 0; i < 6; i++)
  {
    g_rfid_key.keyByte[i] = 0xFF;
  }
}

void loop(void)
{    
  int current_switch_side = get_active_switch_side();
  if (current_switch_side == 2)
  {
    if (g_last_active_switch != current_switch_side)
    {
      lcd_display_two_lines("RFID Read:", "WAITING CARD.."); 
    }
    g_last_active_switch = current_switch_side;

    if (rfid_wait_card() < 0)
    {
      return;
    }

    if (rfid_read_secret_key() == 0)
    {
      lcd_display_two_lines("RFID Read:", "OK!");
      play_sound(true);
      if (g_last_sent_state)
      {
        g_bt_serial.println(g_enable_key);
      }
      else
      {
        g_bt_serial.println(g_disable_key);
      }
      g_last_sent_state = !g_last_sent_state;
    }
    else
    {
      lcd_display_two_lines("RFID Read:", "FAIL!");
      play_sound(false);
    }
    
    lcd_display_two_lines("RFID Read:", "WAITING CARD..");
  }
  else
  {
    if (g_last_active_switch != current_switch_side)
    {
      lcd_display_two_lines("RFID Write:", "WAITING CARD..");
    }
    g_last_active_switch = current_switch_side;

    if (rfid_wait_card() < 0)
    {
      return;
    }
    
    short signed result;
    short signed was_written = rfid_read_secret_key();
    if (was_written == 0)
    {
      result = rfid_write_secret_key(true);
      lcd_display_two_lines("RFID Write:", result == 0 ? "RESET - OK!" : "RESET - FAIL!");
    }
    else
    {
      result = rfid_write_secret_key(false);
      lcd_display_two_lines("RFID Write:", result == 0 ? "NEW CARD - OK!" : "NEW CARD - FAIL!");
    }
        
    play_sound(result == 0 ? true : false);

    lcd_display_two_lines("RFID Write:", "WAITING CARD..");
  }
  
  g_rfid.PICC_HaltA();
  g_rfid.PCD_StopCrypto1();
}

